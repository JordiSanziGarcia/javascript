package com.codesplai.sjava06c; 
import java.util.Random; 
import java.util.Scanner;
import java.util.InputMismatchException;

public class PiedraPapelTijera { 

    
    // Constante para piedra papel tijeras
    private final static byte GANAR = 1;
    private final static byte PERDER = 0;
    private final static byte EMPATE = 2;
    
    /** * juego piedra , papel o tijera  */ 
    
    public static void jugar(){ 
        Juegos.pideJugador(1); 
        //solo interviene un jugador en este juego 
        
        Juegos.partidas = 5; 
        //partidas por defecto en este juego 

        System.out.println(); 
        System.out.println("PIEDRA, PAPEL O TIJERA:"); 
        System.out.println("************"); 
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre); 
        System.out.println(); 
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas); 
        System.out.println(); 

        Random rnd = new Random(); //bucle de partidas 
        for(int i=0; i<Juegos.partidas; i++){ 
            System.out.print("Piedra (1), Papel (2) o Tijera (3) ? ");  //pedimos apuesta
            byte apuesta = apuestaPiedraPapelTijera() ;           
            
            // obtenemos ganador
            byte ganador = ganadorPiedraPapelTijera(apuesta);

            switch(ganador){ 
                case 0: 
                    System.out.print("Has perdido \n");
                    break; 
                case 1: 
                    System.out.print("Has ganado \n");
                    Juegos.jugador1.ganadas++;
                        break; 
                case 2:
                    System.out.print("Has empatado \n");
                    Juegos.jugador1.empates++;
                        break;
                default: 
                    break; 
            } 

            Juegos.jugador1.partidas++; 
        } 
        
        //creamos string con el resumen final de juego y lo mostramos 
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas); 
        System.out.println(resumen); 
    } 

    // creamos un metodo para la validacion de datos de entrada
    private static byte apuestaPiedraPapelTijera(){ 
        byte valor = 0;
        boolean correct = false;
        while(!correct){   
            try {
                valor = keyboard.nextByte() ;
                    if(valor >= 1  && valor <= 3){
                        correct = true;
                    }
                else{
                    System.out.println("Escribe un numero de las opciones");
                }
            } 
            catch (InputMismatchException e) {
                System.out.println("Escribe el numero de la opciones");
                keyboard.nextLine();
            }
        
        }
        valor--;
        return valor;
    }

    // metodo para la validacion de ganador
    // empate = 2
    // ganar  = 1
    // perder = 0
    private static byte ganadorPiedraPapelTijera(byte jug){ 

        Random rnd = new Random();
        Integer rand = new Integer(rnd.nextInt(3));

        //
        byte[][]  matrizGanar = {{EMPATE,PERDER,GANAR},{GANAR,EMPATE,PERDER},{PERDER,GANAR,EMPATE}};

        byte ord = rand.byteValue(); 

        System.out.print("La maquina ha sacado: ");
        switch (ord) { 
            case 0: 
                System.out.print("Piedra \n");
                break; 
            case 1: 
                System.out.print("Papel \n");
                    break; 
            case 2:
                System.out.print("Tijera \n");
                    break;
            default: 
                break; 
        } 

        return matrizGanar[jug][ord];
    }   


    
}