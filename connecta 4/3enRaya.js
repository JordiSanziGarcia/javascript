//variables globales
var jugador;
var guanyar;

var limitSuperior = 0;
var limitEsquerra = 0;

var casellesGuanyar = 4;        // casella per decidir cuantes caselles necesitem per guanyar
var limitInferior = 6;          // alçada del tauler

var limitDret = 7;              // amplada del tauler


// document ready

$(document).ready(function(){
        tauler();
        init();                         //init
        $("td").click(function(){       // si fem click a una casella
            if(!guanyar){               // si hem guanyat no fem res
                jugada($(this));        // fem la jugada /comprobem y cambiem torn
            }

        });
        
        $("#Reset").click(function(){   //Si fem click al Reset , inicialitzem
            init();
        });
    
});


// init
function tauler() {
    var celda = ""
    for (var i = 0; i < limitInferior ; i++){               //per cada casella inicialitzem a espai en blanc
        celda = celda + "<tr>"
        for (var j = 0; j < limitDret; j++){
            celda = celda + "<td id=td"+j+i+">"+j+i+"</td>";
        }
        celda = celda + "</tr>"
    }
    $("table").append(celda);
}

// init
function init() {
    jugador = 1;                            
    guanyar = 0;
    for (var i = 0; i < limitInferior; i++){               //per cada casella inicialitzem a espai en blanc
        for (var j = 0; j < limitDret; j++){
            $("#td"+j+i).text(" ");
        }
    }

    $("#jugador").text(jugador);                // escribim el jugador
    
}

// torn del jugador
function torn() {                               //cambiem el torn 
    if (jugador == 1)                           
        jugador = 2;
    else
        jugador = 1;
    $("#jugador").text(jugador);                //actualitzem el jugador
}


//jugada
function jugada(obj) {                          //si la casella esta buida escribim segons el jugador
    
    
    //recuperamos la fila i la columna de la casilla
    var res = obj.attr("id").substr(2);
 
    var posOrigX = res.substr(0,1);
    var i = limitInferior - 1;
    var sortir = 0;
    
    while(i>= limitSuperior && !sortir){        // comprobem quina es la ultima casella de la columna ocupada 
        if( $("#td"+posOrigX+i).text()==" "){
            
            
            if(jugador==1){                     // pintem segons el jugador
                $("#td"+posOrigX+i).text("X")
            }
            else{
                $("#td"+posOrigX+i).text("O")
            }
            sortir = 1;    
                
        }
        else{i--;}
        
    }
   if(sortir){     // si hem pintat cambiem el comprobem i cambien torn, si no tornarem a clickar.
    comprobar2($("#td"+posOrigX+i)); 
    torn();
   }
       
}



function comprobar2(obj){

    //recuperamos la fila i la columna de la casilla
    var res = obj.attr("id").substr(2);
    var posOrigX = res.substr(0,1);
    var posOrigY = res.substr(1);

    var i = posOrigX;
    var j = posOrigY;

    var comptador = 0;
    var sortir = 0;

    
    // anem a la primera casella mes a la esquerra igual q la actual
    while( i >= limitEsquerra && !sortir ){
        console.log("orig :" + $("#td"+posOrigX+posOrigY).text() + " idd actual :" +$("#td"+i+j).text())
        if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
            if(i==limitEsquerra){sortir = 1;}else {i--;}
        }
        else { i++; sortir = 1;}
    }
    sortir = 0;
    // quan trobem el inici , recorrem de manera horitzontal per trobar si hem guanat
    while( i < limitDret && !sortir && !guanyar ){
        if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
            comptador++;
            if(comptador == casellesGuanyar){
                guanyar = jugador;
            }
            i++;
        }
        else {sortir = 1;}
        
    }
    
    
    if(!guanyar){
        i = posOrigX;
        comptador = 0;
        sortir = 0;

        // anem a la primera casella mes a dalt igual q la actual
        while( j >= limitSuperior && !sortir ){
            if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
                if(j==limitSuperior){sortir = 1;}else {j--;}
            }
            else { sortir = 1;}
        }
        sortir = 0;
        
        // quan trobem el inici , recorrem de manera horitzontal per trobar si hem guanat
        while( j < limitInferior && !sortir && !guanyar ){
            if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
                comptador++;
                if(comptador == casellesGuanyar){
                    guanyar = jugador;
                }
                j++;
            }
            else {sortir = 1;}
        }

    }
    if(!guanyar){
        // reiniciem variables
        j = posOrigY;
        comptador = 0;
        sortir = 0;

        // comprobem diagonal  "\"
        while( i > limitEsquerra-1 && j >= limitSuperior && !sortir ){
            if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
                if( i==limitEsquerra || j == limitSuperior ){sortir = 1;}else {i--;j--;}
            }
            else { i++;j++; sortir = 1;}

        }
        sortir = 0;
        while( i < limitDret && j < limitInferior && !sortir && !guanyar ){
           if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
                comptador++;
                if(comptador == casellesGuanyar){
                    guanyar = jugador;
                }
                i++;
                j++;
           }
           else {sortir = 1;}
        }

    }
    if(!guanyar){
        // reiniciem variables 
        i = posOrigX;
        j = posOrigY;
        comptador = 0;

        // comprobem diagonal "/"
        while( i <= limitDret && j >= limitSuperior && !sortir ){
            if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
                if( i==limitEsquerra || j == limitSuperior ){sortir = 1;}else {i++;j--;}
            }
            else { i++;j++; sortir = 1;}
            
        }
        while( i >= limitEsquerra && j < limitInferior && !sortir && !guanyar ){
            if ($("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text()){
                comptador++;
                if(comptador == casellesGuanyar){
                    guanyar = jugador;
                }
                i--;
                j++;
            }
            else {sortir = 1;}
        }

    }

    
    // comprobem que quedin caselles lliures

    if(!guanyar){
        var aux = 0;
        var empat = 0;
        i = 0;
        j = 0;
        // comprobem si queden caselles buides
        while( i < limitInferior && !aux ){
            j = 0;
            while (j < limitDret  && !aux){
                if($("#td"+j+i).text() == " "){
                    aux++;
                }

                j++;
            }
            i++;

        }
        // si no hi ha caselles per obrir , hem empatat
        if(!aux){
            alert("Empat");
        }
       
    }
    // mostrem jugador que ha guanyat
    else{
        alert("Jugador "+guanyar+" ha guanyat!")
    }

}
