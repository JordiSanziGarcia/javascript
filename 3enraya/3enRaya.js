//variables globales
var jugador;
var guanyar;
var casellesGuanyar = 3;
var limitSuperior = 0;
var limitEsquerra = 0;
var limitInferior = 3;
var limitDret =3;


// document ready
$(document).ready(function(){
        init();                         //init
        $("td").click(function(){       // si fem click a una casella
            if(!guanyar){               // si hem guanyat no fem res
                jugada($(this));        // fem la jugada
                //comprobar();            // comprobem si hem guanyat
                comprobar2($(this));
                torn();                 //cambiem torn
            }

        });
        
        $("#Reset").click(function(){   //Si fem click al Reset , inicialitzem
            init();
        });
    
});

// init
function init() {
    jugador = 1;                            
    guanyar = 0;
    for (var i = 0; i <= 3; i++){               //per cada casella inicialitzem a espai en blanc
        for (var j = 0; j < 3; j++){
            $("#td"+i+j).text(" ");
        }
    }

    $("#jugador").text(jugador);                // escribim el jugador
    
}

// torn del jugador
function torn() {                               //cambiem el torn 
    if (jugador == 1)                           
        jugador = 2;
    else
        jugador = 1;
    $("#jugador").text(jugador);                //actualitzem el jugador
}


//jugada
function jugada(obj) {                          //si la casella esta buida escribim segons el jugador
    if (obj.text() == " ")
        if (jugador == 1)
            obj.text("X");
        else
            obj.text("O");
        
}


function comprobar() {
    
    
    var i = 0;
    var j = 0;
    var aux = 0;
    
    //comprobem files fins a acabarles o guanyar
    while(i < 3 && !guanyar) {

        if ($("#td"+i+"0").text()== $("#td"+i+"1").text() && $("#td"+i+"0").text() == $("#td"+i+"2").text())
            if($("#td"+i+"0").text() != " ")
                guanyar = jugador;
        i++;
    }
    
    //comprobem columnes fins a acabarles o guanyar, si no hem guanyat ja
    
    while(j < 3 && !guanyar) {
 
        if ( ($("#td0"+j).text()== $("#td1"+j).text() ) && ($("#td0"+j).text() == $("#td2"+j).text() ) )
            if($("#td0"+j).text() != " "){ 
               guanyar = jugador; 
            }
                
        j++;
    }
    
    //comprobem diagonals fins a acabarles o guanyar, si no hem guanyat ja
    if(!guanyar){
        if ($("#td00").text() == $("#td11").text() && $("#td00").text() == $("#td22").text()){
                var id = $("#td"+i+"0").text()
                if($("#td11").text() != " ")
                    guanyar = jugador;
        }
        if ($("#td20").text() == $("#td11").text() && $("#td20").text() == $("#td02").text()){
                if($("#td11").text() != " ")
                    guanyar = jugador;
        }
    }
    if (guanyar == 0){                               //si no hem guanyat comprobem que quedin caselles lliures
        for (var i = 0; i <= 3; i++)
            for (var j = 0; j < 3; j++)
                if ($("#td"+i+j).text() == " ")
                    aux++; 
        if (aux==0)                                 //mirem si hem empatat
            alert("Empat");
    }
    else
        alert("Jugador "+guanyar+" ha guanyat!")
            
        
    
}



function comprobar2(obj){

    //recuperamos la fila i la columna de la casilla
    var res = obj.attr("id").substr(2);
    var posOrigX = res.substr(0,1);
    var posOrigY = res.substr(1);

    var i = posOrigX;
    var j = posOrigY;

    var comptador = 0;

    while( i > limitEsquerra && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() ){
        i--;
    }
    while( i < limitDret && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() && !guanyar ){
        comptador++;
        if(comptador == casellesGuanyar){
            guanyar = jugador;
        }
        i++;
    }
    if(!guanyar){
        i = posOrigX;
        comptador = 0;


        while( j > limitSuperior && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() ){
            j--;
        }
        while( j < limitInferior && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() && !guanyar ){
            comptador++;
            if(comptador == casellesGuanyar){
                guanyar = jugador;
            }
            j++;
        }

    }
    if(!guanyar){
        // reiniciem variables
        j = posOrigY;
        comptador = 0;

        // comprobem diagonal  "\"
        while( i > limitEsquerra && j > limitSuperior && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() ){
            i--;
            j--;
        }
        while( i < limitDret && j < limitInferior && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() && !guanyar ){
            comptador++;
            if(comptador == casellesGuanyar){
                guanyar = jugador;
            }
            i++;
            j++;
        }

    }
    if(!guanyar){
        // reiniciem variables 
        i = posOrigX;
        j = posOrigY;
        comptador = 0;

        // comprobem diagonal "/"
        while( i < limitDret && j > limitSuperior && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() ){
            i++;
            j--;
        }
        while( i >= limitEsquerra && j < limitInferior && $("#td"+i+j).text() == $("#td"+posOrigX+posOrigY).text() && !guanyar ){
            comptador++;
            if(comptador == casellesGuanyar){
                guanyar = jugador;
            }
            i--;
            j++;
        }

    }

    // comprobem que quedin caselles lliures

    if(!guanyar){
        var aux = 0;
        var empat = 0;
        i = 0;
        j = 0;
        // comprobem si queden caselles buides
        while( i < limitDret && !aux ){
            j = 0;
            while (j < limitInferior  && !aux){
                if($("#td"+i+j).text() == " "){
                    aux++;
                }

                j++;
            }
            i++;

        }
        // si no hi ha caselles per obrir , hem empatat
        if(!aux){
            alert("Empat");
        }
       
    }
    // mostrem jugador que ha guanyat
    else{
        alert("Jugador "+guanyar+" ha guanyat!")
    }

}
